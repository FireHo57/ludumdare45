﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {

	private static float EPSILON = 0.02f;
	[SerializeField]
	Transform m_gravitySource;

	Rigidbody rigidbody;

	[SerializeField]
	private float MoveSpeed;
	[SerializeField]
	private float RotateSpeed;
	[SerializeField]
	private float JumpSpeed;

	[SerializeField]
	float slowdown = 1.0f;

	[SerializeField]
	float g = 9.81f;

	[SerializeField]
	float raycastRange;

	private Vector3 moveDirection = Vector3.zero;

	private GameObject m_attached = null;
	private Vector3 m_offset;
	private Vector3 last_force = new Vector3(0,0,0);

	private GameObject highlightedObject;
	[SerializeField]
	private Color highlightColour = Color.red;
	private Color originalColour;

	[SerializeField]
	Color heldColor = Color.yellow;
	GameObject heldObject = null;
	Rigidbody heldObjectRigid = null;
	Color originalHeldColor;
	bool holding = false;
	[SerializeField]
	float startHoldLength = 5.0f;
	float holdLength;
	[SerializeField]
	float maxHoldLength;

	[SerializeField]
	float startHoldUp = 5.0f;
	float holdUp;
	[SerializeField]
	float maxHoldup;
	[SerializeField]
	float upDownSpeed = 2.0f;
	[SerializeField]
	float forwardBackwardSpeed = 2.0f;

	private RigidbodyConstraints freezeConstraints = RigidbodyConstraints.FreezePositionX |
													 RigidbodyConstraints.FreezePositionY |
													 RigidbodyConstraints.FreezePositionZ |
													 RigidbodyConstraints.FreezeRotationX |
													 RigidbodyConstraints.FreezeRotationY |
													 RigidbodyConstraints.FreezeRotationZ;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		rotateAround();
		handlePlayerInput();
		CheckForHighlight();
		MoveHeldObject();
		
	}

	void rotateAround()
	{
		//get the mouse rotation
		float mouseRotation = RotateSpeed*Input.GetAxis("Mouse X");

		transform.RotateAround(transform.position, transform.up, mouseRotation);

		float horizontalInput = Input.GetAxis("Horizontal");
		float verticalInput =  Input.GetAxis("Vertical");
		Vector3 horizontalAxis = Vector3.Cross(transform.up,transform.right);
		Vector3 verticalAxis = Vector3.Cross(transform.up,transform.forward);

		bool applyMove = (horizontalInput>0.0f+EPSILON) || (horizontalInput<0.0f-EPSILON) ||
		                 (verticalInput>0.0f+EPSILON)   ||  (verticalInput<0.0f-EPSILON); 

		transform.RotateAround(m_gravitySource.position, verticalAxis, verticalInput*MoveSpeed);
		transform.RotateAround(m_gravitySource.position, horizontalAxis, horizontalInput*MoveSpeed);
	}

	void handlePlayerInput()
	{
		if(Input.GetKeyDown("space"))
		{
			rigidbody.AddRelativeForce(Vector3.up*JumpSpeed, ForceMode.Impulse);
		}

		if (Input.GetMouseButtonDown(0))
		{
			if( heldObject != null )
			{
				DropHeldObject();
			}
			else if( highlightedObject != null)
			{
				PickupSelected();
			}
		}
		else if(Input.GetMouseButtonDown(1))
		{
			if( heldObject != null )
			{
				FixObject();
			}
		}	
	}

	void DropHeldObject()
	{
		heldObject.GetComponent<GravityObject>().SetEnabled(true);
		heldObject.GetComponent<Renderer>().material.color = originalHeldColor;
		heldObject = null;
		heldObjectRigid = null;
		holding = false;
	}

	void PickupSelected()
	{
		heldObject = highlightedObject;
		originalHeldColor = originalColour;
		highlightedObject = null;
		heldObjectRigid = heldObject.GetComponent<Rigidbody>();
		heldObjectRigid.constraints = RigidbodyConstraints.None;
		heldObject.GetComponent<GravityObject>().SetEnabled(false);
		heldObject.GetComponent<Renderer>().material.color = heldColor;
		holding = true;
		holdLength = startHoldLength;
		holdUp = startHoldUp;
		SetInitialPosition();
	}

	void SetInitialPosition()
	{
		heldObjectRigid.MovePosition(transform.position+transform.forward*holdLength+transform.up*holdUp);
	}
	void MoveHeldObject()
	{
		if(!holding) return;
		float deltaX = Input.mouseScrollDelta.y*forwardBackwardSpeed;
		float deltaY = Input.GetAxis("Mouse Y")*upDownSpeed;
		holdLength+=deltaX;
		if( deltaY > EPSILON + 0.0f || deltaY < 0.0f - EPSILON )
		{
			holdUp+=deltaY;
		}
		if( holdLength > maxHoldLength )
		{
			holdLength = maxHoldLength;
		}
		if( holdUp > maxHoldup )
		{
			holdUp = maxHoldup;
		}
		if( holdLength < 1.0f )
		{
			holdLength = 1.0f;
		}
		if( holdUp < 1.0f )
		{
			holdUp = 1.0f;
		}
		
		Vector3 position = transform.position+transform.forward*holdLength+transform.up*holdUp;

		heldObjectRigid.MovePosition(position);
		heldObject.transform.rotation = transform.rotation;
	}

	void FixObject()
	{
		heldObjectRigid.constraints = freezeConstraints;
		DropHeldObject();
	}
	void CheckForHighlight()
	{
		if(holding) return;
		RaycastHit centre;
		RaycastHit up;
		RaycastHit down;

		Vector3 centreVec = transform.forward;
		Vector3 upVec = Quaternion.AngleAxis(-5, transform.right)*transform.forward;
		Vector3 downVec = Quaternion.AngleAxis(5, transform.right)*transform.forward;
		
		bool centreHit = Physics.Raycast(transform.position, centreVec, out centre, raycastRange);
		bool upHit = Physics.Raycast(transform.position, upVec, out up, raycastRange);
		bool downHit = Physics.Raycast(transform.position, downVec, out down, raycastRange);

		if( centreHit )
		{
			 Debug.DrawRay(transform.position, transform.forward * centre.distance, Color.yellow);
			 SetHighlight(centre.transform.gameObject);
		}
		else if( upHit )
		{
			Debug.DrawRay(transform.position, upVec * up.distance, Color.red);
			SetHighlight(up.transform.gameObject);
			
		}
		else if( downHit)
		{
			Debug.DrawRay(transform.position, downVec * down.distance, Color.green);
			SetHighlight(down.transform.gameObject);
		}
		else
		{
			Debug.DrawRay(transform.position, transform.forward * 30, Color.blue);
			if( highlightedObject != null )
			{
				UnHighlight();
			}
		}
	}

	void SetHighlight(GameObject hit)
	{
		if( highlightedObject == null )
		{
			highlightedObject = hit;
			Renderer rend = hit.GetComponent<Renderer>();
			originalColour = rend.material.color;
			rend.material.color = highlightColour;
		}
		else if( hit != highlightedObject)
		{
			UnHighlight();
			highlightedObject = hit;
			Renderer rend = hit.GetComponent<Renderer>();
			originalColour = rend.material.color;
			rend.material.color = highlightColour;
		}
	}

	void UnHighlight()
	{
		Renderer rend = highlightedObject.GetComponent<Renderer>();
		rend.material.color = originalColour;
		highlightedObject = null;
	}
}
