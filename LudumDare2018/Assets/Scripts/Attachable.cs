﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attachable : MonoBehaviour
{

    [SerializeField]
    private float m_shootForce;

    private bool m_attached;
    private Vector3 m_offset;
    private Rigidbody m_rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        m_rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetMouseButtonDown(0))
        {
            m_attached = false;
            Vector3 direction = transform.parent.forward;
            m_rigidbody.AddForce(m_shootForce*direction);
            transform.parent = null;
        }
        else if(m_attached)
        {
            transform.position = transform.parent.position + m_offset;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player")
        {
            m_attached = true;
            transform.parent = other.gameObject.transform;
            Debug.Log(transform.parent);
            m_offset = transform.position - other.gameObject.transform.position;
        }
    }
}
