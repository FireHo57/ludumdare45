﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	[SerializeField]
	private Transform lookat;
	
	[SerializeField]
	private Transform camTransform;
	
	[SerializeField]
	private float distance;

	[SerializeField]
	float m_sensitivity;

	private Camera cam;
	private float currentX = 0.0f;
	private float currentY = 0.0f;

	Transform playerTransform;

	private const float Y_ANGLE_MIN = 5.0f;
	private const float Y_ANGLE_MAX = 40.0f;
	// Use this for initialization
	void Start () {
		camTransform = transform;
		cam = Camera.main;
		GameObject player = GameObject.FindWithTag("Player");
		playerTransform = player.transform;
	}
	
	private void Update()
	{
		currentY += Input.GetAxis("Mouse Y")*m_sensitivity;
		currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
		transform.RotateAround(lookat.position, Vector3.right, 1*m_sensitivity);
	}
/*
	/// <summary>
	/// LateUpdate is called every frame, if the Behaviour is enabled.
	/// It is called after all Update functions have been called.
	/// </summary>
	private void LateUpdate()
	{
		 Vector3 dir = new Vector3(0,0,-distance);
		 Quaternion rotation = Quaternion.Euler( currentY,playerTransform.rotation.x,0);
		 camTransform.position = lookat.position + rotation*dir;
		 camTransform.LookAt(lookat);
	}*/
}
