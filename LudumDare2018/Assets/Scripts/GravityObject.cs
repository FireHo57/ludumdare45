﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GravityObject : MonoBehaviour
{
    [SerializeField]
    float g = 9.81f;

    [SerializeField]
    Transform gravitySource;

    bool m_enabled = true;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        if(m_enabled)
        {
            //this is from the second gravity object to the first so the direction of the force vector is correct
            Vector3 direction = (gravitySource.position - transform.position).normalized;
            Vector3 acceleration = direction * g * GetComponent<Rigidbody>().mass;

            rb.AddForce(acceleration, ForceMode.Acceleration);
        }
    }

    public void SetEnabled( bool enabled )
    {
        m_enabled = enabled;
    }
}
