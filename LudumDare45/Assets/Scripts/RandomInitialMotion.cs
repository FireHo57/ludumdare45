﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RandomInitialMotion : MonoBehaviour
{
    [SerializeField]
    [Range(0,50)]
    float m_startingVelocity;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 randomDirection = new Vector3(Random.value, 0, Random.value);
        Quaternion look = Quaternion.LookRotation(randomDirection);
        transform.rotation = look;
        Debug.Log("rotation: "+transform.rotation);
        Debug.Log(transform.forward);
        Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        rb.AddForce(transform.forward*m_startingVelocity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
