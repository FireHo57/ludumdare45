﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public Transform lookat;
	public Transform camTransform;
	public float distance;
	private Camera cam;
	private float currentX = 0.0f;
	private float currentY = 0.0f;

	private const float Y_ANGLE_MIN = 5.0f;
	private const float Y_ANGLE_MAX = 40.0f;
	// Use this for initialization
	void Start () {
		camTransform = transform;
		cam = Camera.main;
	}
	
	private void Update()
	{
		//currentX += Input.GetAxis("Mouse X")*getRotationController().sensitivityX;
		currentY += Input.GetAxis("Mouse Y")*getRotationController().sensitivityY;
		currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);

	}

	/// <summary>
	/// LateUpdate is called every frame, if the Behaviour is enabled.
	/// It is called after all Update functions have been called.
	/// </summary>
	private void LateUpdate()
	{
		 Vector3 dir = new Vector3(0,0,-distance);
		 Quaternion rotation = Quaternion.Euler(currentY,0,0);
		 camTransform.position = lookat.position + rotation*dir;
		 camTransform.LookAt(lookat);
	}

	private RotationController getRotationController()
	{
		return GameObject.Find("RotationController").GetComponent<RotationController>();
	}
}
