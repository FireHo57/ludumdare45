﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour {

	CharacterController characterController;
	public float MoveSpeed;
	public float RotateSpeed;
	public float JumpSpeed;

	private Vector3 moveDirection = Vector3.zero;

	// Use this for initialization
	void Start () {
		characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		// get frame rate
		RotateAndMove();
	}

	private void RotateAndMove()
	{
		//transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) *Time.deltaTime * RotateSpeed);
		moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical") );
		moveDirection = transform.TransformDirection(moveDirection);
		characterController.Move(moveDirection*Time.deltaTime*MoveSpeed);
	}
}
