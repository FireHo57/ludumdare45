﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepController : MonoBehaviour {

	enum BehaviourState
	{
		FIND_FOOD,
		GRAZE,
		RUNAWAY
	}
	public float speed;
	public float moveTimeout;
	float timeDelta;
	CharacterController characterController;
	// Use this for initialization
	void Start () {
		characterController = GetComponent<CharacterController>();
		timeDelta = 0;
	}
	
	// Update is called once per frame
	void Update () {
		timeDelta+=Time.deltaTime;
		if( timeDelta>= moveTimeout )
		{
			timeDelta=0;
			RandomMove();
			print("Position: "+transform.position);
		}
	}

	void RandomMove()
	{
		float newX = Random.Range(-speed,speed);
		float newZ = Random.Range(-speed,speed);
		characterController.Move(new Vector3(newX,0.0f,newZ));
	}

}
